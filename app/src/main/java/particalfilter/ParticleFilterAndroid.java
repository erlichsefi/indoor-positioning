package particalfilter;

import android.content.Context;
import android.graphics.Bitmap;
import android.graphics.Canvas;
import android.graphics.Color;
import android.graphics.Paint;
import android.support.v7.app.AppCompatActivity;

import com.google.android.gms.maps.GoogleMap;

import java.util.Arrays;
import java.util.Date;

import edu.mwong38calpoly.mapsdemo.Building;
import edu.mwong38calpoly.mapsdemo.Logger;
import edu.mwong38calpoly.mapsdemo.R;

/**
 * Created by erlichsefi on 17-Jan-17.
 */
public class ParticleFilterAndroid {
    public  static Logger prtiaclLog=null;

    private Particle_Filter particle_filter;
    private Building building;
    /*
     * values to draw
     */
    private Bitmap bmOverlay =null;
    private Canvas canvas=null;
    private Paint paint=null;
    public double _norm =1;
    private Point3D Position_By_Wifi=null;
    private  GoogleMap nMAP;
    private Bitmap image;
    private int ParticalSize;
    private double WidthMeterToPixel;
    private double heightMeterToPixel;

        public ParticleFilterAndroid(AppCompatActivity app,Context context, Building _building, GoogleMap _nMAP){
            building=_building;
            nMAP=_nMAP;
            prtiaclLog=new Logger(building.getName()+"_"+new Date().toString(),app);
            //load the clean image
            Grid_Map  gm = new Grid_Map(context,_building.getImage());
            image= gm.getBitMap(context);
            //get the ratio bits to meters
            WidthMeterToPixel=(gm.width())/building.WidthDist();
            heightMeterToPixel=(gm.height())/building.HeightDist();
            //set the particle size- the max size of one meter
            ParticalSize=(int)Math.ceil(Math.min(WidthMeterToPixel, heightMeterToPixel)/2);
            particle_filter= new Particle_Filter(new Point3D(0,0,0), new Point3D(gm.width(), gm.height(),0), gm);
            prtiaclLog.appendLog("new paritcal filter as created of building= "+building.getName());
            prtiaclLog.appendLog("Width of image= "+gm.width()+" height of image= "+gm.height());
            prtiaclLog.appendLog("WidthMeterToPixel factor is= "+WidthMeterToPixel+" heightMeterToPixel facor is= "+heightMeterToPixel);
            prtiaclLog.appendLog("ParticalSize= "+ParticalSize);
            prtiaclLog.appendAsterisks();
            drawOnCanvas();
            building.drawImage(bmOverlay, nMAP);

        }


    public void addStep(double azimuth,double stepSize){
        //compute the angle between the building angle to the North
        double[] ll1=new double[]{building.getSouthWestCorner().latitude,building.getSouthWestCorner().longitude,0};
        double[] ll2=new double[]{building.getNorthWestCorner().latitude,building.getNorthWestCorner().longitude,0};
        double beta= Cords.azmDist(ll1, ll2)[0];
        //angle between north to the direction of step
        double angle=CordsUtil.deg2Rad(azimuth-(beta));
        // the change values in pixels
        double[] change=  new double[]{heightMeterToPixel*Math.cos(angle),WidthMeterToPixel *-Math.sin(angle),0};
       //update  particle filter
        prtiaclLog.appendLog("Step detectd to azimuth "+azimuth+"  with step size ="+stepSize+" and converted to change of ="+ Arrays.toString(change));
        particle_filter.step(new Point3D(change));
        drawOnCanvas();
        building.drawImage(bmOverlay, nMAP);

    }

    public void addWifi(double lat,double lng,double alt, double acc){
        if (building.LoactionInBound(lat, lng)) {
            double[] xyz = building.findLoactionInBuilding(lat, lng,WidthMeterToPixel,heightMeterToPixel);
            prtiaclLog.appendLog("Wifi detected In bound, lat= "+lat+" lng= "+lng+" alt= "+alt+" accuracy= "+acc+" converted to location ="+Arrays.toString(xyz));
            Position_By_Wifi=new Point3D(xyz);
            particle_filter.eval_wifi(new WiFi_Position(new Point3D(Position_By_Wifi), acc));
            drawOnCanvas();
            building.drawImage(bmOverlay, nMAP);
       }
        else{
            prtiaclLog.appendLog("Wifi detected out of bound, lat= "+lat+" lng= "+lng+" alt= "+alt+" accuracy= "+acc);
        }
    }





    private void drawOnCanvas() {
        //create new canvas
        bmOverlay = image.copy(Bitmap.Config.ARGB_8888, true);
        canvas = new Canvas(bmOverlay);
        paint = new Paint();

        paint.setColor(Color.RED);
        Point3D wifi = Position_By_Wifi;
        if(wifi!=null) {
            paint.setColor(Color.GRAY);
            int rad = R.integer.particle_wifi_radius;
            canvas.drawCircle(wifi.ix() - rad, wifi.iy() - rad, rad * 2, paint);
            Position_By_Wifi=null;
        }
        paint.setColor(Color.RED);
        Particles parts = particle_filter.getParts();
        for(int i=0;i<parts.size();i++) {
            Particle curr = parts.get(i);
            drawParticle(curr);
        }
        Point3D best = particle_filter.best(60, 0.9);
        paint.setColor(Color.GREEN);
        drawPoint(best, 5, true);
    }

    private void drawParticle(Particle part) {
        drawPoint(part.get_pos(),ParticalSize, true);
    }

    private void drawPoint(Point3D ps, int rad, boolean fill) {
        int x = (int) (ps.x()*_norm);
        int y = (int) (ps.y()*_norm);
        if (!fill) {
            canvas.drawCircle(x - rad, y - rad, rad * 2, paint);
        } else
            canvas.drawCircle(x - rad, y - rad, rad * 2, paint);
    }

    public void closeLog(){
        prtiaclLog.Close();
    }



}
