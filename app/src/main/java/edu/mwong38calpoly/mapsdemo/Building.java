package edu.mwong38calpoly.mapsdemo;

import android.graphics.Bitmap;
import android.location.Location;

import com.google.android.gms.maps.CameraUpdate;
import com.google.android.gms.maps.CameraUpdateFactory;
import com.google.android.gms.maps.GoogleMap;
import com.google.android.gms.maps.model.BitmapDescriptor;
import com.google.android.gms.maps.model.BitmapDescriptorFactory;
import com.google.android.gms.maps.model.GroundOverlay;
import com.google.android.gms.maps.model.GroundOverlayOptions;
import com.google.android.gms.maps.model.LatLng;
import com.google.android.gms.maps.model.LatLngBounds;

import particalfilter.Cords;
import particalfilter.CordsUtil;

/**
 * Created by DBM on 11/16/2016.
 */
public class Building {
    /**
     * the zoom value , larger -> more zoom
     */
    final float ZoomValue=18;

    private String BuildingName =null;
    /**
     * the photo of the mall
     */
    private String BuildingImage=null;
    /**
     * needed to place the photo on google map
     */
    private LatLng SouthWestCorner=null;
    private LatLng NorthWestCorner=null ;
    /**
     * needed to compute angles and more for particle filter
     *
     */
    private LatLng SouthEastCorner=null ;
    private LatLng NorthEastCorner=null;
    /**
     * needed to zoom in
     */
    private LatLng Center=null;

    /**
     * WARNING!
     * the caclultion doen't include rotation.
     * we assume the rotation is ZERO
     */
    private final int rotation=0;
    /**
     * the last overlay
     */
    private GroundOverlay lastOverLay=null;

    public Building(String _name, LatLng _SouthWestCorner,LatLng _NorthEastCorner,LatLng _NorthWestCorner, LatLng _SouthEastCorner, LatLng _Center,String _image) {
        BuildingName = _name;
        Center=_Center;
        BuildingImage=_image;
        SouthWestCorner=_SouthWestCorner;
        SouthEastCorner=_SouthEastCorner;
        NorthEastCorner=_NorthEastCorner;
        NorthWestCorner=_NorthWestCorner;
    }

    /**
     * get a overlay to place on google maps
     * @return
     */
    private GroundOverlayOptions  getOverLay(){
        try {
            LatLngBounds newarkBounds = new LatLngBounds(
                    SouthWestCorner,       // South west corner
                    NorthEastCorner);      // North east corner
            BitmapDescriptor a=BitmapDescriptorFactory.fromAsset(BuildingImage);

            GroundOverlayOptions newarkMap = new GroundOverlayOptions()
                    .image(a).bearing(rotation)
                    .positionFromBounds(newarkBounds);
            return newarkMap;
        }
        catch (Exception e){
            e.printStackTrace();
        }

        return null;
    }


    /**
     * place the building on the map
     * and zoom in
     * @param mMap Google map
     * @return a pointer to the overlay (for use of remove )
     */
    public GroundOverlay overLayBuilding(GoogleMap mMap) {
        CameraUpdate update = CameraUpdateFactory.newLatLngZoom(Center, ZoomValue);
        GroundOverlay a= mMap.addGroundOverlay(getOverLay());
        mMap.moveCamera(update);
        return a;
    }

    /**
     * the bounds of the building
     * (SouthWestCorner,NorthEastCorner)
     * @return
     */
    public LatLngBounds getBounds() {
        return new LatLngBounds(
                SouthWestCorner,
                NorthEastCorner);
    }


    public void drawImage(Bitmap bmImage, GoogleMap mMap) {
        BitmapDescriptor image = BitmapDescriptorFactory.fromBitmap(bmImage);
        //image = BitmapDescriptorFactory.fromResource(R.drawable.radaraimage9);
        //Set Bounds
        LatLngBounds bounds = getBounds();
        // Append Overlay to map - image appears as white square
        if (lastOverLay!=null){
            lastOverLay.remove();
        }
        lastOverLay=  mMap.addGroundOverlay(new GroundOverlayOptions()
                .image(image).bearing(rotation)
                .positionFromBounds(bounds));


    }
    public String getName() {
        return BuildingName;
    }


    public String getImage() {
        return BuildingImage;
    }

    public LatLng getSouthWestCorner() {
        return SouthWestCorner;
    }

    public LatLng getNorthEastCorner() {
        return NorthEastCorner;
    }
    public LatLng getNorthWestCorner() {
        return NorthWestCorner;
    }

    public double WidthDist(){
        double[] ll1=new double[]{NorthEastCorner.latitude,NorthEastCorner.longitude};
        double[] ll2=new double[]{NorthWestCorner.latitude,NorthWestCorner.longitude};
        return Math.abs(CordsUtil.DistanceLatLonToLatLonInMeters(ll1, ll2));
    }

    public double HeightDist(){
        double[] ll1=new double[]{SouthEastCorner.latitude,SouthEastCorner.longitude};
        double[] ll2=new double[]{SouthWestCorner.latitude,SouthWestCorner.longitude};
        return Math.abs(CordsUtil.DistanceLatLonToLatLonInMeters(ll1, ll2));
    }

    public double[] findLoactionInBuilding(double lat,double lng,double WidthMeterToPixel,double heightMeterToPixel){

        double[] ll1=new double[]{NorthWestCorner.latitude,NorthWestCorner.longitude,0};
        double[] ll2=new double[]{lat,lng,0};
        double alph= Cords.azmDist(ll1, ll2)[0];
        ll1=new double[]{NorthWestCorner.latitude,NorthWestCorner.longitude,0};
        ll2=new double[]{NorthEastCorner.latitude,NorthEastCorner.longitude,0};
        double beta= Cords.azmDist(ll1, ll2)[0];
        double angle=CordsUtil.deg2Rad(alph - beta);

        Location startPoint=new Location("locationA");
        startPoint.setLatitude(NorthWestCorner.latitude);
        startPoint.setLongitude(NorthWestCorner.longitude);

        Location endPoint=new Location("locationA");
        endPoint.setLatitude(lat);
        endPoint.setLongitude(lng);
        double A=startPoint.distanceTo(endPoint);
        double B=Math.sin(angle)*A;
        double Bp=B*heightMeterToPixel;
        double distance=Bp/Math.sin(angle) ;
        return new double[]{distance*Math.sin(angle),distance*Math.cos(angle),0};
    }





    public boolean LoactionInBound(double lat,double lng){
        LatLng SouthWest=getSouthWestCorner();
        LatLng NorthEast=getNorthEastCorner();
        return lat>SouthWest.latitude && lat<NorthEast.latitude &&
                lng>SouthWest.longitude && lng<NorthEast.longitude  ;
    }

}
