package edu.mwong38calpoly.mapsdemo;

import android.Manifest;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.net.Uri;
import android.os.Bundle;
import android.os.Handler;
import android.support.v4.app.ActivityCompat;
import android.support.v4.content.ContextCompat;
import android.support.v7.app.AppCompatActivity;
import android.view.View;
import android.widget.AdapterView;
import android.widget.Button;
import android.widget.CheckBox;
import android.widget.Spinner;
import android.widget.Toast;

import com.google.android.gms.appindexing.Action;
import com.google.android.gms.appindexing.AppIndex;
import com.google.android.gms.common.api.GoogleApiClient;
import com.google.android.gms.maps.GoogleMap;
import com.google.android.gms.maps.OnMapReadyCallback;
import com.google.android.gms.maps.SupportMapFragment;
import com.google.android.gms.maps.UiSettings;
import com.google.android.gms.maps.model.BitmapDescriptorFactory;
import com.google.android.gms.maps.model.GroundOverlay;
import com.google.android.gms.maps.model.LatLng;
import com.google.android.gms.maps.model.Marker;
import com.google.android.gms.maps.model.MarkerOptions;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.Date;

import particalfilter.CordsUtil;
import particalfilter.ParticleFilterAndroid;
import services.GoogleServiceLocation;
import services.MyLocation;
import services.ServiceResultReceiver;
import services.StepAzimuteService;

/**
 * TODO: All 360 image stuff should be moved to its own class.
 */
public class MapsActivity extends AppCompatActivity implements OnMapReadyCallback,GoogleMap.OnMyLocationButtonClickListener,ActivityCompat.OnRequestPermissionsResultCallback, ServiceResultReceiver.Receiver {
    public  static Logger systemlog=null;

    private GoogleMap mMap;
    private UiSettings mUiSettings;
    private ServiceResultReceiver result;
    private CheckBox mMyLocationButtonCheckbox;
    private CheckBox CompassCheckbox;
    private CheckBox ZoomCheckbox;
    private int stepSize= R.integer.step_size;
    private  BuildingFactory buildingFactory;
    private static final int MY_LOCATION_PERMISSION_REQUEST_CODE = 1;
    private static final int LOCATION_PERMISSION_REQUEST_CODE = 1;
    private boolean mPermissionDenied = false;
    ParticleFilterAndroid particalFilter;
 long lastUpdate;
    double[]  StartingLocation=null;
    double[] lastNetworkLocation=null;
    double[] lastGPSLocation=null;
    boolean mapReady=false;
    public static boolean mIsInForegroundMode;
    Building mall;
    ArrayList<Marker> markersOnMap;
    /**
     * ATTENTION: This was auto-generated to implement the App Indexing API.
     * See https://g.co/AppIndexing/AndroidStudio for more information.
     */
    private GoogleApiClient client;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        lastUpdate=System.currentTimeMillis();
        systemlog=new Logger(new Date().toString(),MapsActivity.this);
        setContentView(R.layout.activity_maps);
        mMyLocationButtonCheckbox = new CheckBox(this);
        CompassCheckbox=new CheckBox(this);
        ZoomCheckbox=new CheckBox(this);
        SupportMapFragment mapFragment =
                (SupportMapFragment) getSupportFragmentManager().findFragmentById(R.id.map);
        mapFragment.getMapAsync(this);
        // ATTENTION: This was auto-generated to implement the App Indexing API.
        // See https://g.co/AppIndexing/AndroidStudio for more information.
        client = new GoogleApiClient.Builder(this).addApi(AppIndex.API).build();
        buildingFactory=new BuildingFactory(this);
        result = new ServiceResultReceiver(new Handler());
        result.setReceiver(this);
        markersOnMap= new ArrayList<Marker>();
                SetMallSelection();

    }

    @Override
    protected void onResume() {
        super.onResume();
        mIsInForegroundMode = true;
    }



    /**
     * This is where we can add markers or lines, add listeners or move the camera. In this case,
     * we
     * just add a marker near Africa.
     */
    @Override
    public void onMapReady(GoogleMap map) {
        mMap=map;
        mMap.setOnMyLocationButtonClickListener(this);
        mUiSettings = mMap.getUiSettings();
        SetLoactions();
        enableMyLocation();
        startServices();

        // Keep the UI Settings state in sync with the checkboxes.
        mUiSettings.setZoomControlsEnabled(ZoomCheckbox.isEnabled());
        mUiSettings.setCompassEnabled(CompassCheckbox.isEnabled());
        mUiSettings.setMyLocationButtonEnabled(mMyLocationButtonCheckbox.isEnabled());
        mapReady=true;

    }



    public void SetLoactions(){
        Intent serviceIntent = new Intent(MapsActivity.this, GoogleServiceLocation.class);
        Bundle bundle = new Bundle();
        bundle.putParcelable(ServiceResultReceiver.RESULT_RECEIVER_EXTRA_L, result);
        serviceIntent.putExtras(bundle);
        startService(serviceIntent);


    }

    public void startServices() {
        Intent serviceIntent=new Intent(MapsActivity.this, StepAzimuteService.class);
        Bundle bundle = new Bundle();
        bundle.putParcelable(ServiceResultReceiver.RESULT_RECEIVER_EXTRA_L, result);
        serviceIntent.putExtras(bundle);
        startService(serviceIntent);
    }


    public void SetMallSelection(){
        Spinner dropDown = (Spinner) findViewById(R.id.dropDown);
        final GroundOverlay[] lestOverLay = {null};
        dropDown.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
                if (lestOverLay[0] != null)
                    lestOverLay[0].remove();

                String p = (String) parent.getSelectedItem();
                //int ids=getResources().getIdentifier(pVariableName, pResourcename, pPackageName);
                mall = buildingFactory.getBuilding(p);
                if (mall != null) {
                    lestOverLay[0] = mall.overLayBuilding(mMap);
                    particalFilter = new ParticleFilterAndroid(MapsActivity.this,getApplicationContext(), mall, mMap);


                }
            }

            @Override
            public void onNothingSelected(AdapterView<?> parent) {

            }
        });

        Button button=(Button) findViewById(R.id.restart);
        button.setOnClickListener(new View.OnClickListener() {


            @Override
            public void onClick(View arg0) {
                if (particalFilter != null) {
                    particalFilter.closeLog();
                    particalFilter = new ParticleFilterAndroid(MapsActivity.this,getApplicationContext(), mall, mMap);
                    lestOverLay[0] = mall.overLayBuilding(mMap);
                    if (lastGPSLocation!=null){
                        StartingLocation = Arrays.copyOf(lastGPSLocation, lastGPSLocation.length);
                    }
                    for (Marker m : markersOnMap) {
                        m.remove();
                    }
                }

            }

        });


        Button offbutton=(Button) findViewById(R.id.off);
        offbutton.setOnClickListener(new View.OnClickListener() {

            @Override
            public void onClick(View arg0) {
                systemlog.Close();
                stopServices();
                if (particalFilter!=null) particalFilter.closeLog();
                try {
                    Thread.sleep(1000);
                } catch (InterruptedException e) {
                    e.printStackTrace();
                }
                System.exit(0);
            }

        });
    }


    @Override
    public void onStepDetected(int resultCode, Bundle resultData) {
                // Handle response from IntentService here
                float azimut = resultData.getFloat("azimut");
                float NumberOfsteps = resultData.getFloat("step");
                    //start Caculting steps from the first Gps Locatin
                    if (lastGPSLocation != null && StartingLocation == null) {
                        StartingLocation = Arrays.copyOf(lastGPSLocation, lastGPSLocation.length);

                    }
                      if (StartingLocation != null) {
                          StartingLocation = CordsUtil.offsetLatLonAlt(StartingLocation, azimut, stepSize * (NumberOfsteps));
                         Marker m= mMap.addMarker(new MarkerOptions()
                                  .position(new LatLng(StartingLocation[0], StartingLocation[1])).title(NumberOfsteps + "" + azimut).icon(BitmapDescriptorFactory.fromAsset("dot.png")));
                          markersOnMap.add(m);
                          systemlog.appendLog("step deteced to  azimuth= "+azimut);
                          systemlog.appendLog(" new Coordinates are: " + Arrays.toString(StartingLocation));
                    }

                    if (particalFilter!=null) {
                        systemlog.appendLog("the above step sent to partical filler");

                        particalFilter.addStep(azimut, stepSize);
                    }
                    Toast.makeText(this, "Step detectd To "+azimut , Toast.LENGTH_SHORT).show();
                    if (mapReady && StartingLocation!=null) {

                    }
    }
    @Override
    public  void onLocationRecive(int resultCode, Bundle resultData){
        long numberOfSecond=5L;
        MyLocation loc = resultData.getParcelable(ServiceResultReceiver.Location_EXTRA);
            //if got loaction from Wifi update.
          if (particalFilter!=null && loc.isNetwork() && (lastUpdate+1000*numberOfSecond)<System.currentTimeMillis() ) {
            systemlog.appendLog("On Loaction poped, got: "+loc+" sent to pratical fillter");
            particalFilter.addWifi(loc.getLatLon().latitude, loc.getLatLon().longitude, loc.getAltitude(), loc.getError());
            lastUpdate=System.currentTimeMillis();
          }

        if (loc.isNetwork()){
            lastNetworkLocation=new double[]{loc.getLatLon().latitude, loc.getLatLon().longitude, loc.getAltitude()};
        }
        else if (loc.isGps()){
            lastGPSLocation=new double[]{loc.getLatLon().latitude, loc.getLatLon().longitude, loc.getAltitude()};
        }

        if (mapReady && loc.ErrorLowerThen(5)) {
            systemlog.appendLog(" the above location showen on map ");
             mMap.addMarker(new MarkerOptions()
                    .position(loc.getLatLon()).title("" + loc.getProvider()).icon(BitmapDescriptorFactory.fromAsset("gps.png")));
            Toast.makeText(this, "Location gotten from: "+loc.getProvider(), Toast.LENGTH_SHORT).show();

        }
    }







    /**
     * Returns whether the checkbox with the given id is checked.
     */
    private boolean isChecked(int id) {
        return ((CheckBox) findViewById(id)).isChecked();
    }

    public void setMyLocationButtonEnabled(View v) {
        if (!checkReady()) {
            return;
        }
        // Enables/disables the my location button (this DOES NOT enable/disable the my location
        // dot/chevron on the map). The my location button will never appear if the my location
        // layer is not enabled.
        // First verify that the location permission has been granted.
        if (ContextCompat.checkSelfPermission(this, Manifest.permission.ACCESS_FINE_LOCATION)
                == PackageManager.PERMISSION_GRANTED) {
            mUiSettings.setMyLocationButtonEnabled(mMyLocationButtonCheckbox.isChecked());
        } else {
            // Uncheck the box and request missing location permission.
            mMyLocationButtonCheckbox.setChecked(false);
            ActivityCompat.requestPermissions(MapsActivity.this,
                    new String[]{
                            Manifest.permission.ACCESS_FINE_LOCATION
                    },
                    100);
        }
    }



    /**
     * Enables the My Location layer if the fine location permission has been granted.
     */
    private void enableMyLocation() {
        if (ContextCompat.checkSelfPermission(this, Manifest.permission.ACCESS_FINE_LOCATION)
                != PackageManager.PERMISSION_GRANTED) {
            // Permission to access the location is missing.
                ActivityCompat.requestPermissions(MapsActivity.this,
                        new String[]{
                                Manifest.permission.ACCESS_FINE_LOCATION
                        },
                        100);

        } else if (mMap != null) {
            // Access to the location has been granted to the app.
            mMap.setMyLocationEnabled(true);
        }
    }

    @Override
    public boolean onMyLocationButtonClick() {
        Toast.makeText(this, "MyLocation button clicked", Toast.LENGTH_SHORT).show();
        // Return false so that we don't consume the event and the default behavior still occurs
        // (the camera animates to the user's current position).
        return false;
    }

//
//    @Override
//    public void onRequestPermissionsResult(int requestCode, @NonNull String[] permissions,
//                                           @NonNull int[] grantResults) {
//        if (requestCode != LOCATION_PERMISSION_REQUEST_CODE) {
//            return;
//        }
//
//        if (PermissionUtils.isPermissionGranted(permissions, grantResults,
//                Manifest.permission.ACCESS_FINE_LOCATION)) {
//            // Enable the my location layer if the permission has been granted.
//            enableMyLocation();
//        } else {
//            // Display the missing permission error dialog when the fragments resume.
//            mPermissionDenied = true;
//        }
//    }


    /**
     * Checks if the map is ready (which depends on whether the Google Play services APK is
     * available. This should be called prior to calling any methods on GoogleMap.
     */
    private boolean checkReady() {
        if (mMap == null) {
            Toast.makeText(this, "Map Not Ready", Toast.LENGTH_SHORT).show();
            return false;
        }
        return true;
    }

//    /**
//     * Requests the fine location permission. If a rationale with an additional explanation should
//     * be shown to the user, displays a dialog that triggers the request.
//     */
//    public void requestLocationPermission(int requestCode) {
//        if (ActivityCompat.shouldShowRequestPermissionRationale(this,
//                Manifest.permission.ACCESS_FINE_LOCATION)) {
//            // Display a dialog with rationale.
//            PermissionUtils.RationaleDialog
//                    .newInstance(requestCode, false).show(
//                    getSupportFragmentManager(), "dialog");
//        } else {
//            // Location permission has not been granted yet, request it.
//            PermissionUtils.requestPermission(this, requestCode,
//                    Manifest.permission.ACCESS_FINE_LOCATION, false);
//        }
//    }

    @Override
    public void onStart() {
        super.onStart();

        // ATTENTION: This was auto-generated to implement the App Indexing API.
        // See https://g.co/AppIndexing/AndroidStudio for more information.
        client.connect();
        Action viewAction = Action.newAction(
                Action.TYPE_VIEW, // TODO: choose an action type.
                "Maps Page", // TODO: Define a title for the content shown.
                // TODO: If you have web page content that matches this app activity's content,
                // make sure this auto-generated web page URL is correct.
                // Otherwise, set the URL to null.
                Uri.parse("http://host/path"),
                // TODO: Make sure this auto-generated app deep link URI is correct.
                Uri.parse("android-app://edu.mwong38calpoly.mapsdemo/http/host/path")
        );
        AppIndex.AppIndexApi.start(client, viewAction);
    }



    // Method to stop the service
    public void stopServices() {
        stopService(new Intent(getBaseContext(), StepAzimuteService.class));
        if (result != null) {
            result.setReceiver(null);
        }
        stopService(new Intent(getBaseContext(), GoogleServiceLocation.class));
        if (result != null) {
            result.setReceiver(null);
        }
    }

    @Override
    protected  void onDestroy(){
        stopServices();
        super.onDestroy();


    }

    @Override
    protected void onPause() {
        super.onPause();
        mIsInForegroundMode = false;
    }


    @Override
    public void onStop() {
        super.onStop();
        // ATTENTION: This was auto-generated to implement the App Indexing API.
        // See https://g.co/AppIndexing/AndroidStudio for more information.
        Action viewAction = Action.newAction(
                Action.TYPE_VIEW, // TODO: choose an action type.
                "Maps Page", // TODO: Define a title for the content shown.
                // TODO: If you have web page content that matches this app activity's content,
                // make sure this auto-generated web page URL is correct.
                // Otherwise, set the URL to null.
                Uri.parse("http://host/path"),
                // TODO: Make sure this auto-generated app deep link URI is correct.
                Uri.parse("android-app://edu.mwong38calpoly.mapsdemo/http/host/path")
        );
        AppIndex.AppIndexApi.end(client, viewAction);
        client.disconnect();
    }
}
