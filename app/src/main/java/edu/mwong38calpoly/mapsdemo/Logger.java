package edu.mwong38calpoly.mapsdemo;

import android.Manifest;
import android.content.pm.PackageManager;
import android.os.Environment;
import android.support.v4.app.ActivityCompat;
import android.support.v4.content.ContextCompat;
import android.support.v7.app.AppCompatActivity;

import java.io.BufferedWriter;
import java.io.File;
import java.io.FileWriter;
import java.io.IOException;
import java.util.Date;

/**
 * Created by erlichsefi on 19-Jan-17.
 */
public class Logger {
    File logFile;
    BufferedWriter buf;

    public Logger(String logName,AppCompatActivity mp){
              if (PackageManager.PERMISSION_GRANTED!=ContextCompat.checkSelfPermission(mp,
                      Manifest.permission.WRITE_EXTERNAL_STORAGE)){
                  ActivityCompat.requestPermissions(mp,
                          new String[]{
                                  Manifest.permission.READ_EXTERNAL_STORAGE,
                                  Manifest.permission.WRITE_EXTERNAL_STORAGE
                          },
                          100);
              }

                File fullPATH = Environment.getExternalStoragePublicDirectory(
                        Environment.DIRECTORY_DOCUMENTS);
        logFile = new File(fullPATH,logName+".txt");
        if (!logFile.exists())
        {
            try
            {
                fullPATH.mkdirs();
                logFile.createNewFile();
                 buf = new BufferedWriter(new FileWriter(logFile, true));

            }
            catch (IOException e)
            {
                e.printStackTrace();
            }
        }
    }


    public void appendLog(String text)
    {
        try
        {
            buf.append(new Date().toString() +" : "+ text);
            buf.newLine();
        }
        catch (IOException e)
        {
            // TODO Auto-generated catch block
            e.printStackTrace();
        }
    }

    public void Close(){
        try
        {
            buf.close();
        }
        catch (IOException e)
        {
            // TODO Auto-generated catch block
            e.printStackTrace();
        }
    }

    public void appendAsterisks(){
        try
        {
            buf.append("****************************************************8");
            buf.newLine();
        }
        catch (IOException e)
        {
            // TODO Auto-generated catch block
            e.printStackTrace();
        }
    }
}
