package edu.mwong38calpoly.mapsdemo;

import android.content.Context;

import com.google.android.gms.maps.model.LatLng;

import java.util.HashMap;
import java.util.Map;

/**
 * erlichsefi
 */
//Eventual helper class to generate all our wonderful buildings... "wonderful"
public class BuildingFactory {
     Map<String,Building> buildings = new HashMap<String,Building>();

    public BuildingFactory(Context _context){
        //Don't touch them
        LatLng sw= new LatLng(   32.171335,     34.926893);
        LatLng ne= new LatLng(    32.172811,      34.929429);
        LatLng nw=new LatLng( 32.172812, 34.926895) ;
        LatLng se=new LatLng(32.171336,34.929425) ;
        LatLng cen= new LatLng(32.1718621,34.9287211);
        String name=_context.getResources().getString(R.string.kfarsabaG);
        buildings.put(name,new Building(name,sw,ne,nw,se,cen,"GkfarSaba.png"));

         sw= new LatLng(    32.102502 ,    35.209168);
         ne= new LatLng(         32.103114,        35.209986);
         nw=new LatLng(       32.103114,      35.209168) ;
         se=new LatLng(     32.102502,    35.209986) ;

         cen= new LatLng( 32.102818, 35.209577);
         name=_context.getResources().getString(R.string.kcg);
        buildings.put(name,new Building(name,sw,ne,nw,se,cen,"kcgr.png"));





    }

    public  Building getBuilding(String name){
        for (Map.Entry<String, Building> a:buildings.entrySet()) {
            if (a.getKey().equals(" "+name+" ")){
                return a.getValue();
            }
        }
        return null;
    }


}












































