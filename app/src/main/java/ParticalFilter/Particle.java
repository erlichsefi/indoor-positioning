/**
 * This class represents a simple particle with weight & position 
 */
package particalfilter;

public class Particle {
	public static final double INIT_WEIGHT = 2.0;
	private double _w;
	public double _temp_sum_w;
	private Point3D _pos;
	
	public Particle(Point3D p) {
		set_pos(new Point3D(p));
		set_weight(INIT_WEIGHT);
	}
	public Particle(Particle p) {
		set_pos(new Point3D(p.get_pos()));
		set_weight(p.get_weight());
	}
	public Point3D get_pos() {
		return _pos;
	}

	public void set_pos(Point3D _pos) {
		this._pos = _pos;
	}

	public double get_weight() {
		return _w;
	}

	public void set_weight(double _w) {
		this._w = _w;
	}
	public void action(Point3D p) {
		_pos.offset(p);
	}
	public String toString() {return "Prt: "+this.get_pos()+"  w: "+this.get_weight();}
}
