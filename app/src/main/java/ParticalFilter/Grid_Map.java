package particalfilter;

import android.content.Context;
import android.content.res.AssetManager;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.Color;

import java.io.IOException;
import java.io.InputStream;

/**
 * This class represents a 2D indoor map with go / nogo limitations. The map is used by
 * the particle filter for grading the particles w.r.t. to the ground map.
 * WHITE = 0 = OK to go
 * BLACK = 100 = NO Go!!
 * GRAY = 50 = LOW prob. GO.
 *  
 * @author boaz
 *
 */
public class Grid_Map {
	private int[][] _map;
	String file;
	final double GS_RED = 0.35;

	final double GS_GREEN = 0.55;

	final double GS_BLUE = 0.1;
	
	public Grid_Map(Context context,String image_file_name) {
		file=image_file_name;
		init(context,image_file_name);
	}

	private void init(Context context,String file) {
			Bitmap image = getBitMap(context);
			int w = image.getWidth();
			int h = image.getHeight();
			_map = new int[w][h];

			for(int y=0;y<h;y++) {
				for(int x=0;x<w;x++) {
					int  R, G, B,Pixel;
					Pixel = image.getPixel(x, y);
					R = Color.red(Pixel);
					G = Color.green(Pixel);
					B = Color.blue(Pixel);
					_map[x][y] = (int)(GS_RED * R + GS_GREEN * G + GS_BLUE * B);


				}
		}

	}
	
	
	public int width() {return _map.length;}
	public int height() {return _map[0].length;}

	public int get(int x, int y) {
		if(x>=0 && y>=0 && x<width() && y<height()) {
			return  this._map[x][y];
		}
		return -1;
	}


	public Bitmap getBitMap(Context context) {
		AssetManager assetManager = context.getAssets();
		InputStream istr;
		Bitmap bitmap = null;
		try {
			istr = assetManager.open(file);
			bitmap = BitmapFactory.decodeStream(istr);
		} catch (IOException e) {
			// handle exception
		}
		return bitmap;
	}
}