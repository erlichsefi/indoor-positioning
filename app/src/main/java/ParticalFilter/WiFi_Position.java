package particalfilter;

public class WiFi_Position {
	private Point3D _pos;
	private double _accuracy; // typical [2,7] meters
	private double _close_enough; // typical 0.5*_accuracy
	public WiFi_Position(Point3D p, double acc) {
		_pos = new Point3D(p);
		this._accuracy = acc;
		_close_enough = _accuracy *0.5;
	}
	public Point3D get_pos() {
		return _pos;
	}

	public double distance_for_weight(Point3D p) {
		double d = p.distance3D(_pos);
		double ans = Math.max(0, d-_close_enough);
		return ans;
	}

	//public void set_conf(double _conf) {
	//	this._conf = _conf; }
	public double get_accuracy() {
		return _accuracy;
	}
	//public void set_accuracy(double _accuracy) {
		//this._accuracy = _accuracy;}
	
	
	
}
