/**
 * This class represents a set of particles as a basis for Particle Filter
 * Note:
 * 1. The parameters for the PF - are all defined in the PF class - and they have been optimized
 * for this case (Image / size / simulation) it will probably WILL NOT work fine for a real world scenario:
 * All the parameters should be redefined and tested!
 */
package particalfilter;

import java.util.ArrayList;
import java.util.Arrays;

public class Particles {
	private ArrayList<Particle> _parts;

	public Particles(int size, Point3D min, Point3D max) {
		_parts = new ArrayList<Particle>(size);
		for(int i=0;i<size;i++) {
			Particle p = new Particle(randomPoint(min,max));
			_parts.add(p);
		}
	}
	public int size() {return _parts.size();}
	
	public void shake(Point3D min, Point3D max) {
		for(int i=0;i<size();i++) {
			Point3D mv = randomPoint(min,max);
			_parts.get(i).action(mv);
		}
	}
	public void action(Point3D p) {
		for(int i=0;i<size();i++) {
			_parts.get(i).action(p);
		}
	}
	public Particle best() {
		Particle ans = this._parts.get(0);
		for(int i=1;i<size(); i++) {
			Particle p = this._parts.get(i);
			if(p.get_weight()> ans.get_weight()) {ans = p;}
		}
		return ans;
	}
	public Point3D best(double dist, double close) {
		Particle best = best();
		Point3D ans = new Point3D(0,0,0);
		double w = best.get_weight()*close;
		double count = 0;
		for(int i=0;i<size(); i++) {
			Particle p = this._parts.get(i);
			if(p.get_weight()> w && p.get_pos().distance3D(best.get_pos())<dist) {
				count++;
				ans.offset(p.get_pos());
			}
		}
		ans.factor(1.0/count);
		return ans;
	}
	public Particle get(int i) {
		return this._parts.get(i);
	}
	public void order_weights() {
		double ws=0;
		for(int i=0;i<size();i++) {
			Particle pr = _parts.get(i);
			ws += pr.get_weight();
		}
		double norm = ws/size();
		ws=0;
		for(int i=0;i<size();i++) {
			Particle pr = _parts.get(i);
			double w = pr.get_weight()/norm;
			ws+=w;
			pr.set_weight(w);
			pr._temp_sum_w = ws;
		}
	}
	public int findPrt(double w) {
		int ans = -1;
		double last = 0;
		double up = 0;
		for(int i=0;i<size(); i++) {
			up += _parts.get(i).get_weight();
			if(last<=w && up> w) {
				ans = i;
			}
			last = up;
		}
		return ans;
	}
	/**this is the MAIN TRICK - should be tested carefully!! **/
	public void resample(int size, Point3D min, Point3D max) {
		ArrayList<Particle> temp = new ArrayList<Particle>(size());
		if(size<0 || size > size()) {size = size();}
		double[] ww = new double[size];
		double norm = 1;
		if(size>0) {norm = size(); norm = norm/size;}
		for(int i=0;i<size;i++) {
			//ww[i] = random(0,size);
			ww[i] = i*norm+0.5;  // USING Roi's safe circle prob.
		}
		Arrays.sort(ww);
		int a = 0;
		
		for(int i=0;i<size;i++) {
			while(a<size && ww[a]<this._parts.get(i)._temp_sum_w){
				temp.add(new Particle(_parts.get(i)));
				a++;
			}
		}
		while(temp.size()<size()) {
			Point3D p = randomPoint(min,max);
			Particle pr = new Particle(p);
			pr.set_weight(0.5);
			temp.add(pr);
		}
		this._parts = temp;
	}
	public String toString() {
		String ans = "Particles: \n";
		for(int i=0;i<size();i++) {
			ans += this.get(i).toString()+"\n";
		}
		return ans;
	}
	private Point3D randomPoint(Point3D min, Point3D max) {
		double x = random(min.x(), max.x());
		double y = random(min.y(), max.y());
		double z = random(min.z(), max.z());
		return new Point3D(x,y,z);
	}
	private double random(double min, double max) {
		double ans = min;
		double dx = max-min;
		ans += dx*Math.random();
		return ans;
	}
}

