/**
 * This class represents a simple variant of Particle Filter for localization - with memory
 * 
 * Todo:
 * 1. Move all the constants to a single - (Utils) class
 * 2. Write some accuracy testing w.r.t. a given scenario - for test the expected accuracy error
 * 3. Parameter optimization
 * 4. Generalize the code for 3D - 2.5D (several floors)
 * 5. The particles should be sorted by weight
 * 6. Each particle should also have an absolute weight and a generation (the iteration in which it was created)
 * 7. 
 * 
 * Known Bugs:
 * 1. Resample (particles) is bugy!! in particular the (WiFi part)
 * 2. 
 */
package particalfilter;


public class Particle_Filter {
	private Particles _parts;
	private Point3D _minROI, _maxROI;
	private Grid_Map _gm;
	public final double _norm = 1.02;  // for eval
	public  final double PixelsToParticalRatio=0.0005;


	public Particle_Filter( Point3D min, Point3D max, Grid_Map gm) {
		this._minROI = new Point3D(min);
		this._maxROI = new Point3D(max);
		int _size=(int) (gm.height()*gm.width()*PixelsToParticalRatio);
		_parts = new Particles(_size, min, max);
		_gm = gm;
	}

	public int size(){
		return this._parts.size();
	}
	
	public void eval_map() {
		for(int i=0;i<size();i++) {
			Particle p = _parts.get(i);
			Point3D pos = p.get_pos();
			int c = _gm.get(pos.ix(), pos.iy());
			double w = p.get_weight();

			if(c>240) { //OK to go
				p.set_weight(w*_norm);
				//			System.out.println("WHITE! !!!!!!!!!!!!!!");
			}
			else if(c!=-1) { // Obstacle
				p.set_weight(w/_norm);
			}

			else{ // out of map
				p.set_weight(w/_norm);
			}
		}
		_parts.order_weights();
	}


	public void eval_wifi(WiFi_Position wp) {
		Point3D pos = wp.get_pos(); 
		double acc = wp.get_accuracy();

		Point3D min =  new Point3D(pos.x()-acc, pos.y()-acc, pos.z());
		Point3D max =  new Point3D(pos.x()+acc, pos.y()+acc, pos.z());
		for(int i=0;i<size();i++) {
			Particle p = _parts.get(i);
			double dist =wp.distance_for_weight(p.get_pos());
			double w = p.get_weight();
			if(dist>acc) { // Obstacle
				p.set_weight(w/2);
			}
			else if(dist>acc/2) { //OK to go
				p.set_weight(w/1.4);  // 2^{0.5}
			}
			else if(dist>0) { 
				p.set_weight(w/1.2);  // 2^{0.25}
			}
			else if(dist==0){ // with in the accuracy level
				p.set_weight(w*1.2);
			}
		}
		_parts.order_weights();
		_parts.resample(_parts.size()/ 2, min, max);
		acc /= 10;
		Point3D m0 = new Point3D(-acc,-acc,0), m1 = new Point3D(+acc,+acc,0);
		_parts.shake(m0, m1);

	}
	public void step(Point3D p) {
		this._parts.action(p);
	//	this._parts.shake(_minROI, _maxROI);
		this.eval_map();
		this._parts.resample(size(), _minROI, _maxROI);
	}

	public Particle best() {return this._parts.best();}
	public Point3D best(double dist, double close_enogh) {return this._parts.best(dist, close_enogh);}
	public Point3D get_maxROI() {
		return _maxROI;
	}

	public void set_maxROI(Point3D _maxROI) {
		this._maxROI = _maxROI;
	}

	public Point3D get_minROI() {
		return _minROI;
	}
	public Particles getParts() {return this._parts;}
	public void set_minROI(Point3D _minROI) {
		this._minROI = _minROI;
	}


}
