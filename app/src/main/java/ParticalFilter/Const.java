package particalfilter;
/**
this class contains all the public constants of the Ex6 example. <br>
*/ 
public class Const {
	public static final int Gen=0, Point=1, Rect1=2, Rect2=3,
		PATH=4,SENSOR2=5,SENSOR5=6,SENSOR10=7,T3=8, Poly1=50, Poly2=51;

	public static final int Fire=10,RectS1=11,RectS2=12,
		SIM=13,Grid_Algo2=14,Circle_Algo = 131, Circle_Algo10=132, Circle_Algo100=133,Copy1=15,Copy2=16,Delete=17,_Color=18,Rect5a=19,Rect5b=20,
				Rect10a=21,Rect10b=22, Rect20a=23,Rect20b=24,
			By_area=1,By_perimeter=2,By_2S=0;
	public static double K= 1000, M=K*K, G = M*K;
	public static double EPS1 = 1/M;
	public final static int MAX_X = 300;
	public final static int MAX_Y = 300;
	public static String APPLICATION_TITLE = "Fire Detection Simulation";
	public static final double _epsilon = 0.000001;
	public final static int black=0,red=1,blue=2,green=3,pink=4,gray=5;
	public final static int Algo_Grid=1, Algo_Circle = 2;
	public final static double POS_ERR=8, TOF_ERR=0.6;

	public static final int Target_View = 135;

	public static final double YAW_ERR = 0.2;
	public static final double PITCH_ERR = 0.4;

	public static final double FoV = 4; // degrees [-4,+4]

//	/** the color table - converts int to 'real' (awt) Color.*/
//	public static Color color(int i) {
//		Color c = Color.black;
//		if(i==green) c = Color.green;
//		else if(i==red) c = Color.red;
//		else if(i==blue) c = Color.blue;
//		else if(i==pink) c = Color.pink;
//		else if(i==gray) c = Color.gray;
//		return c;
//	}
}
