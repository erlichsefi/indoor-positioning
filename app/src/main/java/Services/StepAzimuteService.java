package services;

import android.app.IntentService;
import android.content.Intent;
import android.hardware.Sensor;
import android.hardware.SensorEvent;
import android.hardware.SensorEventListener;
import android.hardware.SensorManager;
import android.os.Bundle;
import android.os.IBinder;
import android.os.ResultReceiver;
import android.widget.Toast;

import edu.mwong38calpoly.mapsdemo.MapsActivity;

/**
 * Created by erlichsefi on 13-Jan-17.
 */
public class StepAzimuteService extends IntentService  {
    private SensorManager mSensorManager;
    private  Sensor accelerometer;
    private  Sensor magnetometer;
    private  Sensor mstep;

    private long lastUpdate;
    SensorEventListener listen;
    ResultReceiver result;
    float[] gData;
    float[] mData;
    float azimut;

    public StepAzimuteService() {
        super(StepAzimuteService.class.getName());

    }
    @Override
    public void onCreate() {
        if (!MapsActivity.mIsInForegroundMode) {
            Toast.makeText(getApplicationContext(), "Started", Toast.LENGTH_SHORT).show();
        }
                    mSensorManager = (SensorManager) getSystemService(SENSOR_SERVICE);
            accelerometer = mSensorManager.getDefaultSensor(Sensor.TYPE_ACCELEROMETER);
            magnetometer = mSensorManager.getDefaultSensor(Sensor.TYPE_MAGNETIC_FIELD);
            mstep = mSensorManager.getDefaultSensor(Sensor.TYPE_STEP_DETECTOR);

            super.onCreate();
    }
    @Override
    public IBinder onBind(Intent intent) {
        return null;
    }
    @Override
    public int onStartCommand(Intent intent, int flags, int startId) {
        Bundle bundle = intent.getExtras();
        if (result==null ) {

            result = bundle.getParcelable(ServiceResultReceiver.RESULT_RECEIVER_EXTRA_L);
        }
        mSensorManager = (SensorManager) getApplicationContext()
                .getSystemService(SENSOR_SERVICE);
        listen = new SensorListen();
        mSensorManager.registerListener(listen, accelerometer, SensorManager.SENSOR_DELAY_FASTEST);
        mSensorManager.registerListener(listen, magnetometer, SensorManager.SENSOR_DELAY_FASTEST);
        mSensorManager.registerListener(listen, mstep, SensorManager.SENSOR_DELAY_FASTEST);
        return START_STICKY;
    }



    @Override
    protected void onHandleIntent(Intent workIntent) {
        if (result==null ) {
            result = workIntent.getExtras().getParcelable(ServiceResultReceiver.RESULT_RECEIVER_EXTRA_L);
        }
    }

    @Override
    public void onDestroy() {
        mSensorManager.unregisterListener(listen);
        Toast.makeText(this, "StepAzimuteService Destroy", Toast.LENGTH_SHORT).show();
        super.onDestroy();
    }
class SensorListen implements SensorEventListener{
float NumberOfStepsSOFar=0;
float StepSent=0;

    @Override
    public void onSensorChanged(SensorEvent event) {
        lastUpdate = System.currentTimeMillis();
        if (event.sensor.getType() == Sensor.TYPE_ACCELEROMETER)
            gData = event.values;
        if (event.sensor.getType() == Sensor.TYPE_MAGNETIC_FIELD)
            mData = event.values;
        if (event.sensor.getType() == Sensor.TYPE_STEP_DETECTOR)
            NumberOfStepsSOFar++;

        if (gData != null && mData != null ) {
            float iMat[] = new float[9];
            float rMat[] = new float[9];
            float orientation[] = new float[9];
            if ( SensorManager.getRotationMatrix( rMat, iMat, gData, mData ) ) {
                azimut= (int) ( Math.toDegrees( SensorManager.getOrientation( rMat, orientation )[0] ) + 360 ) % 360;
            }
        }
        if (NumberOfStepsSOFar>StepSent) {
            Bundle b = new Bundle();
            b.putFloat("azimut", azimut);
            b.putFloat("step", (NumberOfStepsSOFar-StepSent));
            result.send(ServiceResultReceiver.WIFI_STEP_RESULT, b);
            StepSent=NumberOfStepsSOFar;
        }
    }


    @Override
        public void onAccuracyChanged(Sensor sensor, int accuracy) {

        }

    }



}