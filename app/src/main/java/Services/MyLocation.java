package services;

import android.os.Parcel;
import android.os.Parcelable;

import com.google.android.gms.maps.model.CircleOptions;
import com.google.android.gms.maps.model.LatLng;
import com.google.android.gms.maps.model.MarkerOptions;

/**
 * Created by erlichsefi on 14-Jan-17.
 */
public class MyLocation implements Parcelable {
    private LatLng someVariable = null;
    private double ciracle;
    private String Provider;
    private  double altitide;
    private int Color;
    String markerName;

    public MyLocation(int _Color, String _markerName){
        Color=_Color;
        markerName=_markerName;
    }

    public MyLocation set(double _ciracle,String _Provider,LatLng _someVariable,double _altitide){
        someVariable=_someVariable;
        ciracle=_ciracle;
        Provider=_Provider;
        altitide=_altitide;
        return this;
    }

    protected MyLocation(Parcel in) {
        someVariable = in.readParcelable(LatLng.class.getClassLoader());
        ciracle = in.readDouble();
        Provider = in.readString();
        Color = in.readInt();
        markerName = in.readString();
        altitide=in.readDouble();


    }


    public synchronized MarkerOptions getMarker() {
        return new MarkerOptions()
                .position(someVariable).title(Provider);
               // .icon(BitmapDescriptorFactory.fromAsset(markerName));
    }

    public synchronized CircleOptions getCircle() {
        return new CircleOptions().center(someVariable).fillColor(Color).radius(ciracle);
    }




    @Override
    public int describeContents() {
        return this.hashCode();
    }
    public static final Parcelable.Creator<MyLocation> CREATOR = new Parcelable.Creator<MyLocation>() {

        public MyLocation createFromParcel(Parcel in) {
            return new MyLocation(in);
        }

        public MyLocation[] newArray(int size) {
            return new MyLocation[size];
        }
    };

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeParcelable(someVariable, flags);
        dest.writeDouble(ciracle);
        dest.writeString(Provider);
        dest.writeInt(Color);
        dest.writeString(markerName);
        dest.writeDouble(altitide);
    }

    public boolean isGps() {
        return Provider.equals("gps");

    }

    public boolean isNetwork() {
        return Provider.equals("network");

    }

    public LatLng getLatLon() {
        return new LatLng(someVariable.latitude,someVariable.longitude);
    }

    public double getError() {
        return ciracle;
    }

    public String getProvider() {
        return Provider;
    }

    public double getAltitude() {
        return altitide;
    }

    public boolean ErrorLowerThen(int i) {
        return ciracle<i;
    }
    @Override
    public String toString(){
        return "lat= "+someVariable.latitude+" lng "+someVariable.longitude
                +" Provider= "+Provider+" altitide= "+altitide+" errorRadius= "+ciracle;

    }

}
