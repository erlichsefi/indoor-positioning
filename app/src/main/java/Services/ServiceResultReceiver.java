package services;

import android.annotation.SuppressLint;
import android.os.Bundle;
import android.os.Handler;
import android.os.ResultReceiver;

/**
 * Created by erlichsefi on 15-Jan-17.
 */
@SuppressLint("ParcelCreator")
public class ServiceResultReceiver extends ResultReceiver {
    public static final int WIFI_STEP_RESULT = 1;
    public static final int GoogleLocationRESULT = 2;
    public static final String Location_EXTRA = "Geolocation";
    public static final String RESULT_RECEIVER_EXTRA_L = "reciever1";
    public static final String RESULT_RECEIVER_EXTRA_S = "reciever2";

    private Receiver mReceiver;

    public ServiceResultReceiver(Handler handler) {
        super(handler);
    }

    public void setReceiver(Receiver receiver) {
        mReceiver = receiver;
    }

    public interface Receiver {
        void onStepDetected(int resultCode, Bundle resultData);
        void onLocationRecive(int resultCode, Bundle resultData);
    }

    @Override
    protected void onReceiveResult(int resultCode, Bundle resultData) {
        if (mReceiver != null) {
            if (resultCode==WIFI_STEP_RESULT) {
                mReceiver.onStepDetected(resultCode, resultData);
            }
            else if (resultCode== GoogleLocationRESULT){
                mReceiver.onLocationRecive(resultCode, resultData);
            }
        }
    }
}