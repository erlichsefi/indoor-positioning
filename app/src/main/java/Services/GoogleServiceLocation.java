package services;

import android.app.IntentService;
import android.content.Intent;
import android.location.Location;
import android.location.LocationListener;
import android.location.LocationManager;
import android.os.Bundle;
import android.os.IBinder;
import android.os.ResultReceiver;
import android.widget.Toast;

import com.google.android.gms.maps.model.LatLng;

import java.util.Observable;
import java.util.Observer;

import edu.mwong38calpoly.mapsdemo.MapsActivity;

/**
 * Created by erlichsefi on 13-Jan-17.
 */
public class GoogleServiceLocation extends IntentService implements Observer {
    private LocationManager locationManager;
    private boolean isGPSEnabled;
    private boolean isNetworkEnabled;
    private LocationListener locationListener;
    private MyLocation LocationTypeToCreateFrom=new MyLocation(000,"wifiMarker.png");

    private  ResultReceiver result;

    private  Location LastNetwork;
     private long lastNetworkUpdate;

    private long lastGPSUpdate;
    private Location LastGPS;

    public GoogleServiceLocation()  {
        super(GoogleServiceLocation.class.getName());
    }

    @Override
    public void onCreate() {
        if (!MapsActivity.mIsInForegroundMode) {
            Toast.makeText(getApplicationContext(), "Started", Toast.LENGTH_SHORT).show();
        }
        locationManager = (LocationManager) getSystemService(LOCATION_SERVICE);
        super.onCreate();

    }

    @Override
    public IBinder onBind(Intent intent) {
        return null;
    }


    @Override
    public int onStartCommand(Intent intent, int flags, int startId) {
        Bundle bundle = intent.getExtras();
        if (result==null ) {

            result = bundle.getParcelable(ServiceResultReceiver.RESULT_RECEIVER_EXTRA_L);
        }

        locationManager = (LocationManager) getSystemService(LOCATION_SERVICE);
        locationListener = new Mylocationlistener();
        checkPermission("android.permission.ACCESS_FINE_LOCATION", 1, 0);
        checkPermission("android.permission.ACCESS_COARSE_LOCATION", 1, 0);

        if (locationManager.isProviderEnabled(LocationManager.GPS_PROVIDER)) {
            locationManager.requestLocationUpdates(LocationManager.GPS_PROVIDER, 0, 0, locationListener);
        }
        if (locationManager.isProviderEnabled(LocationManager.NETWORK_PROVIDER)) {
            locationManager.requestLocationUpdates(LocationManager.NETWORK_PROVIDER, 0, 0, locationListener);
        }
        return START_REDELIVER_INTENT ;
    }


    @Override
    protected void onHandleIntent(Intent workIntent) {
        if (result==null ) {
            result = workIntent.getExtras().getParcelable(ServiceResultReceiver.RESULT_RECEIVER_EXTRA_L);

        }
    }




    @Override
    public void onDestroy() {
        checkPermission("android.permission.ACCESS_FINE_LOCATION", 1, 0);
        locationManager.removeUpdates(locationListener);
        Toast.makeText(this, "GoogleServiceLocation Destroy", Toast.LENGTH_SHORT).show();
        super.onDestroy();
    }



    @Override
    public void update(Observable observable, Object data) {

    }


    class Mylocationlistener implements LocationListener {

        public Mylocationlistener() {
            isGPSEnabled = locationManager
                    .isProviderEnabled(LocationManager.GPS_PROVIDER);
            isNetworkEnabled = locationManager
                    .isProviderEnabled(LocationManager.NETWORK_PROVIDER);

        }

        @Override
        public void onLocationChanged(Location location) {
            //GETING THE DATA
            LatLng loc = new LatLng(location.getLatitude(), location.getLongitude());
            double alt=location.getAltitude();
            int errorR = (int) location.getAccuracy();
            MyLocation loaction= LocationTypeToCreateFrom.set(errorR, location.getProvider(), loc, alt);
            //sending result
            Bundle b = new Bundle();
            b.putParcelable(ServiceResultReceiver.Location_EXTRA, loaction);
            result.send(ServiceResultReceiver.GoogleLocationRESULT,b);

            if (loaction.isGps()){
                LastGPS=location;
                lastGPSUpdate=System.currentTimeMillis();
            } else if (loaction.isNetwork()){
                LastNetwork=location;
                lastNetworkUpdate=System.currentTimeMillis();
            }
        }

        @Override
        public void onStatusChanged(String provider, int status, Bundle extras) {

        }

        @Override
        public void onProviderEnabled(String provider) {

        }

        @Override
        public void onProviderDisabled(String provider) {

        }

    }

}